import pandas

data = pandas.read_excel("2009_QA_1_4866.xlsx")
data = data.drop_duplicates(subset="Name", keep="last")
data.to_excel("result.xlsx", index=None)
