import requests
import json
import pandas as pd
import os
import sys

grammar_dict = pd.read_csv("grammar_filter.csv")["Short Message"].__array__()


def count_errors(text, port):
    try:
        resp = requests.get("http://localhost:" + str(port) + "/v2/check?language=en-US&text=" + str(text))
        result_pre = json.loads(resp.content.decode())["matches"]
        result = []
        for i in result_pre:
            if i["shortMessage"] in grammar_dict:
                print(i)
                result.append(i)
        return result
    except json.decoder.JSONDecodeError:
        return {}


def process_df(path, name, port):
    df = pd.read_excel(path + name)
    df["num_of_errors"] = df["Text"].map(lambda x: len(count_errors(x, port)))
    df["errors_type"] = df["Text"].map(lambda x: str(count_errors(x, port)))
    directory = path + "checked/"
    try:
        os.stat(directory)
    except:
        os.mkdir(directory)
    df.to_excel(path + "checked/" + name, index=None)
    return 0


# java -cp languagetool-server.jar org.languagetool.server.HTTPServer --port 8083
def main():
    # print(sys.argv)
    for filename in os.listdir(sys.argv[1]):
        if filename.endswith(".xlsx"):
            print("Processing " + filename + " ...")
            process_df(sys.argv[1], filename, "8083")
            print("Finished " + filename)
            continue
        else:
            continue


if __name__ == '__main__':
    main()
