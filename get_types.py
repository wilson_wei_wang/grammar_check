import requests
import json
import pandas as pd
import os
import sys


def count_errors(text, port):
    result = []
    try:
        resp = requests.get("http://localhost:" + str(port) + "/v2/check?language=en-US&text=" + str(text))
        for item in json.loads(resp.content.decode())["matches"]:
            result.append([item["message"], item["shortMessage"], text])
    except json.decoder.JSONDecodeError:
        pass
    return result


def process_df(path, name, port):
    df = pd.read_excel(path + name)
    msg = []
    smsg = []
    sent = []
    for text in df["Text"]:
        for data in count_errors(text, port):
            msg.append(data[0])
            smsg.append(data[1])
            sent.append(data[2])
    output_df = pd.DataFrame()
    output_df["Message"] = msg
    output_df["Short Message"] = smsg
    output_df["Sentence"] = sent
    output_df = output_df.drop_duplicates(subset="Short Message", keep="first")
    directory = path + "checked/"
    try:
        os.stat(directory)
    except:
        os.mkdir(directory)
    output_df.to_excel(path + "checked/" + name, index=None)
    return 0


# java -cp languagetool-server.jar org.languagetool.server.HTTPServer --port 8083
def main():
    for filename in os.listdir(sys.argv[1]):
        if filename.endswith(".xlsx"):
            print("Processing " + filename + " ...")
            process_df(sys.argv[1], filename, "8083")
            print("Finished " + filename)
            continue
        else:
            continue


if __name__ == '__main__':
    main()
